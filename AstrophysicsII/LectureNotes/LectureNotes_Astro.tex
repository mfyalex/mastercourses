\documentclass[preprint2,flushrt]{aastex}
\usepackage{amsmath}
\usepackage[version=3]{mhchem}
%\usepackage{fourier}
\usepackage{amsthm}
\usepackage{makeidx}
\usepackage{hyperref}
%\usepackage{mathpazo}
\usepackage{xcolor}
\usepackage{booktabs}
\newcommand{\ten}[2]{#1\times 10^{#2}}
\newcommand{\msun}{\ M_{\sun}}
\newcommand{\notes}[1]{\textcolor{gray}{#1}}
\newcommand{\hii}[1]{\ion{H}{2}}
\newcommand{\hh}{\mathcal{H}}
\newcommand{\dd}{\mathrm{d}}
\newcommand{\ave}[1]{\langle#1\rangle}
%\renewcommand{\normalsize}{\fontsize{9pt}{\baselineskip}\selectfont}
\usepackage[left=1.3in, right=0.6in, bottom=1in, top=1in]{geometry}
\makeatletter %  now normal "letter"
\@addtoreset{equation}{section}
\makeatother %  is restored as "non-letter"
\renewcommand\theequation{{\thesection}%
.{\arabic{equation}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{NOTES ON ASTROPHYSICS II}
\author{\textsc{Fanyi Meng}}
\affil{Universit\"{a}t zu K\"{o}ln}
\email{mfyalex@gmail.com}
\author{Version: \today}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{abstract}
	These lecture notes are based on the Astrophysics II course taught by Prof. Dr. Peter Schilke, in WS 13/14 at {Universit\"{a}t zu K\"{o}ln}. These notes are summarization of the personal handwritten notes. Fanyi Meng copyrights these lecture notes. No part of this document may be reproduced in any form or means, without the prior written consent of Fanyi Meng.\end{abstract}
\keywords{Astrohysics, Graduate Course}
\tableofcontents
\clearpage
\section{INTERSTELLAR MEDIUM}
 \subsection{Structure of ISM, Energy levels}
 \subsubsection{Components of ISM}
  The Milky Way mainly comprises stars, ISM and dark matter, with masses:
  \begin{align}
       M_{tot} &\approx 10^{11}\ M_\sun\\
       M_{\star} &\approx  \ten{5}{10}\ M_\sun\\
       M_{dm} &\approx \ten{5}{10}\ M_\sun
   \end{align} 
   within the mass of baryionic matter ($M_{\star}$), $\sim$10\% are contributed by ISM:
   \begin{equation}
       M_{ISM} \approx \ten{7}{9}\ M_\sun
   \end{equation}
   Within the ISM, $\sim \ten{1.1}{9} M_\sun$ are \hii. 

   ISM comprises gas and dust, gas shared $\sim99\%$ portion of mass. There are various phases of gas exist, as in Table \ref{Tab. Phases of Interstellar Gas}

   \begin{table}[h]
    
    \caption{Phases of Interstellar Gas\label{Tab. Phases of Interstellar Gas}}
\begin{scriptsize}
   \begin{tabular}{llll}
   \\
    \toprule
    \hline
    Phase & $T(\rm K)$ & $n_{\rm H}(\rm cm^{-3})$ & $P(\rm K\ cm^{-3})$\\
    \hline
    Coronal gas (HIM) & $\ga 10^{5.5}$ &$\sim 0.004$ &$1000$\\
    {H}{\textsc{ii}}        & $10^4$         &$0.3-10^4$&$3000-10^8$\\
    WIM               &                & 0.3 &\\
    Warm {H}\textsc{i} (WNM)          & $\sim 5000$    & 0.6 &$3000$\\
    Cool {H}\textsc{i} (CNM) &$\sim 100$ &30&$3000$\\
    Diffuse $\rm H_2$ & $\sim 50$ & $\sim 100$ & $5000$\\
    Dense $\rm H_2$ & $10-50$ & $10^3-10^6$ & $10^4-\ten{5}{7}$\\
    Cool stellar outflows & $50-10^3$ & $1-10^6$ &\\
    \hline
    \bottomrule
    \end{tabular}
    \end{scriptsize} 
    \end{table}

    ISM is far from thermodynamic equilibrium: $T_{bg} = 2.7\rm\ K$; $T_{kin} \sim 10\rm\ K$; $T_{\star} \sim 5000\rm\ K$.

    \subsubsection{Collision Process}
    Although that in some cases three body collisions play important role, the majority of the collisions in ISM are still two-body collision, due to its low number density. The collision process can be in various form: molecular excitation, chemical reaction, ionization, recombination etc..

    Reaction rate per volume can be described as:
    \begin{equation}
        n_An_B\langle\sigma v\rangle_{AB} 
    \end{equation}
    where $v$ is the relative speed of $A$ and $B$, $\sigma_{AB}$ is the cross section for reaction. We have Maxwellian velocity distribution:
    \begin{equation}
        f_v \dd v = 4\pi \left(\frac{\mu}{2\pi kT}\right)^{3/2}e^{{-\mu v^2}/{2kT}}v^2\dd v
    \end{equation}
    where reduced mass $\mu$ is:
    \begin{equation}
        \mu \equiv \frac{m_Am_B}{m_A+m_B}
    \end{equation}
    Considering $f_v \dd v = f_E \dd E$, we have:
    \begin{equation}
        vf_E = vf_v\frac{\dd v}{\dd E} = \left(\frac{8}{\pi \mu k T}\right)^{1/2} \frac{E}{kT} e^{-E/KT}
    \end{equation}
    The average cross section therefore can be obtained:
    \begin{align}
        \langle \sigma v \rangle_{AB} &= \int_0^\infty \sigma_{AB}(v) v f_v\dd v\\
        &=\left(\frac{8kT}{\pi \mu}\right)^{1/2} \int_0^\infty \sigma_{AB}(v) \frac{E}{kT} e^{-E/kT}\frac{\dd E}{kT}
    \end{align}

    \paragraph{The Impact Approximation}
    When particle $Z_2 e$ moves with a constant relative velocity passing $Z_1 e$, and the minimum distance between them is $b$, general distance is $d$, we can write perpendicular force between them 
    \begin{equation}
    F_{\perp} = \frac{Z_1Z_2e^2}{(b/\cos\theta)^2}\cos\theta
    \end{equation}
    where
    \begin{equation}
        \cos\theta = \frac{\vec{b}\cdot \vec{d}}{bd},\ (\theta\in[-\frac{\pi}{2}, \frac{\pi}{2}])
    \end{equation}
    let $v$ be the relative velocity, the differential of time can be expressed as:
    \begin{equation}
         \dd t  = \frac{\dd (b\tan\theta)}{v} = \frac{b}{v\cos^2\theta}\dd \theta
     \end{equation} 
    The integral of momentum that has been transfered by Coulomb force is nothing but:
    \begin{align}\label{Eq. Impact Approx.}
        \Delta p &= \int ^\infty_{-\infty} F_{\perp}\dd t = \frac{Z_1Z_2e^2}{bv_1}\int^{\pi/2}_{-\pi/2}\cos\theta\dd\theta \notag \\
        & = 2\frac{Z_1Z_2e^2}{bv}
    \end{align}

    The parallel transferred momentum vanishes due to the integral. Such an impact actually twists the trajectory of the electron a little bit, the small twisted angle $\varphi$ is:
    \begin{equation}
        \varphi \approx  \tan \varphi =  2\frac{Z_1Z_2e^2}{bv^2m_e} = \frac{|E_{pot}|}{E_{kin}}
    \end{equation}
    Where $|E_{pot}| = Z_1Z_2e^2/b$.
    \paragraph{Example: Collision Ionization}
    We only concern the collision that can be dealt with Impact Approximation, thus the velocity should be high enough. Let $I$ be the ionization energy of a certain target particle with equivalent charge $Ze$. Thus we have:
    \begin{equation}
        \Delta p > \sqrt{2m_e I}
    \end{equation}
    With Impact Approximation, we use $p = p(b,v)$ as in Equation \ref{Eq. Impact Approx.} can get the maximum value of the minimum distance of the moving electron to target:
    \begin{equation}
        b_{max}(v) = \sqrt{\frac{2Z^2e^4}{m_ev^2I}} 
    \end{equation}
    Therefore we obtain the correpsonding cross section:
    \begin{equation}
        \sigma(v) \approx \pi b_{max}^2(v) = \frac{2\pi Z^2e^4}{m_ev^2I}
    \end{equation}
    We should notice the velocity range that is sufficient for ionization:
    \begin{equation}
        v \in (\sqrt{\frac{2I}{m_e}},\infty)
    \end{equation}
    Therefore the thermal rate coeefieicnt can be derived for collosional ionization:
    \begin{align}
        \ave{\sigma v} &= \int_{v_{min}}^{\infty} \sigma(v)vf(v)\dd v\\
        & = \int_{v_{min}}^{\infty} \frac{2\pi Z^2e^4}{m_ev^2I}v4\pi \left(\frac{m_e}{2\pi kT}\right)^{3/2}e^{\frac{-m_e v^2}{2kT}}v^2\dd v\\
        & = Z^2\sqrt{\frac{8\pi}{m_e k T}}\frac{e^4}{I}
    \end{align}

 \subsection{Emission and Absorption, Radiative Transfer}
 \subsubsection{Molecular Energy Levels}
 \paragraph{Molecular Vibration}
 \paragraph{Molecular Rotation}
 \subsubsection{Emission and Absorption Processes} 
 \newtheorem*{absorption}{Absorption} 
 \begin{absorption}
 \begin{equation}
     X_l+h\nu\rightarrow X_u,\ h\nu = E_u-E_l
 \end{equation}
 \end{absorption}
 We have the number density changing rate of both state $l$ and $u$:
 \begin{equation}
     \left(\frac{\dd n_u}{\dd t}\right)_{l\rightarrow u} = -\left(\frac{\dd n_l}{\dd t}\right)_{l\rightarrow u} = n_l B_{lu} u_\nu
 \end{equation}
 where $B_{lu}$ is the Einstein $B$ coefficient and $u_\nu$ is the radiation energy density.

\newtheorem*{spemission}{Spontaneous Emission} 
 \begin{spemission}
 \begin{equation}
     X_u\rightarrow X_l + h\nu, \ h\nu = E_u-E_l
 \end{equation}
 \end{spemission}

 \newtheorem*{stemission}{Stimulated Emission} 
 \begin{stemission}
 \begin{equation}
     X_u+h\nu\rightarrow X_l + 2h\nu, \ h\nu = E_u-E_l
 \end{equation}
 \end{stemission}
 We have the number density changing rate of both state $l$ and $u$:
 \begin{equation}
     \left(\frac{\dd n_l}{\dd t}\right)_{u\rightarrow l} = -\left(\frac{\dd n_u}{\dd t}\right)_{u\rightarrow l} = n_u(A_{ul}+B_{ul} u_\nu)
 \end{equation}
 where $A_{ul}$ is the Einstein $A$ coefficient, depicting the spontaneous emission which is independent of the radiation field.

 To get the radiation energy density, we adopt the Planck's Law under \textbf{LTE assumption}: 
 \newtheorem*{planckslaw}{Planck's Law} 
 \begin{planckslaw}
 \begin{equation}
     B_\nu = \frac{2h\nu^3}{c^2} \frac{1}{e^{{h\nu}/{kT}}-1}
 \end{equation}
 where $B_\nu$ is the power emitted from the emitting surface, per unit projected area of emitting surface, per unit solid angle, per frequency interval.
 \end{planckslaw}
 Thus energy density can be expressed as:
 \begin{align}
     u_\nu &= \frac{E_\nu}{V} \\
     & = \frac{4\pi B_\nu \Delta t \cdot 4\pi r^2}{V}\\ 
     & = \frac{4\pi B_\nu \Delta t \cdot 4\pi r^2}{4\pi R^2 \Delta R}
 \end{align}
 in photon gas, we have:
  \begin{align}
    r&\equiv R\\
   \Rightarrow 
    u_\nu & = \frac{4\pi B_\nu \Delta t }{\Delta R}\\
    & = \frac{4\pi B_\nu}{c}\\
    & = \frac{8\pi h\nu^3}{c^3} \frac{1}{e^{{h\nu}/{kT}}-1}
 \end{align}
 \subsubsection{Radiative Transfer}
    \newtheorem*{specificintensity}{Specific Intensity}
    \begin{specificintensity}
        The electrodynamic power per unit area, with frequency within $[\nu,\nu+\dd\nu]$, propagating in direction $\hat{n}$ with solid angle $\dd\Omega$ is:
        \begin{equation}
            I_\nu(\nu,\hat{n},\vec{r},t)\dd\nu\dd\Omega
        \end{equation}
    \end{specificintensity}
    Adopting the LTE assumption, the dependency of specific intensity of $\nu$ and $T$ can be described by Planck's Law:
    \begin{equation}\label{Eq. Specific Intensity}
         (I_\nu)_{\rm LTE} = B_\nu \equiv \frac{2h\nu^3}{c^2}\frac{1}{e^{h\nu/kT}-1}
    \end{equation} 

    \newtheorem*{photonoccnum}{Photon Occupation Number}
    \begin{photonoccnum}
        \begin{equation}
            n_\gamma(\nu,\hat{n},\vec{r},t) \equiv \frac{c^2}{2h\nu^3} I_\nu(\nu,\hat{n},\vec{r},t)
        \end{equation}
    \end{photonoccnum}
    which can be easily derived from the equation of photon numbers, when we notice each photon occupy volume $(c/\nu)^3$:
    \begin{equation}
        \frac{1}{3}\cdot\frac{\partial}{\partial \nu}\left[\frac{4\pi R^2\dd R}{\left({c}/{\nu}\right)^3}\right] n_\gamma = \frac{I_\nu\dd t \cdot 4\pi R^2}{2h\nu}
    \end{equation}
    If the radiation field is LTE i.e. blackbody, we can have:
    \begin{equation}
        (n_\gamma)_{\rm LTE} = \frac{1}{e^{h\nu/kT}-1}        
    \end{equation}
    \newtheorem*{brightnesst}{Brightness Temperature}
    \begin{brightnesst}
    Solve $T$ from Equation \ref{Eq. Specific Intensity}, we can get brightness temperature:
        \begin{equation}
            T_B = \frac{h\nu/k}{\ln[1+{2h\nu^3}/({c^2I_\nu})]}
        \end{equation}
    which is a real temperature for a blackbody.
    \end{brightnesst}
    \newtheorem*{antennat}{Antenna Temperature}
    \begin{brightnesst}
    In the limitation: $kT \gg h_\nu$ i.e. $n_\gamma \gg 1$, we have:
        \begin{equation}
            \lim_{h\nu/kT_A\rightarrow 0 }I_\nu = \frac{2h\nu^3}{c^2}\frac{kT_A}{h\nu}
        \end{equation}
    thus the $T_A$ can be solved:
        \begin{equation}
            T_A(\nu) = \frac{c^2}{2k\nu^2}I_\nu
        \end{equation}    
    which is a real temperature for a blackbody when fulfill the long wavelength limitation.
    \end{brightnesst}
 \subsection{Continuum Radiation Process}
 \subsection{Fluid Dynamics, Shocks and SNRs}
 \subsection{Shocks, SNRs, HII regions and PDRs}
\section{STAR FORMATION}
 \subsection{Molecular Clouds}

\end{document}