\contentsline {section}{\numberline {1}INTERSTELLAR MEDIUM}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Structure of ISM, Energy levels}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Components of ISM}{2}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Collision Process}{2}{subsubsection.1.1.2}
\contentsline {paragraph}{The Impact Approximation}{2}{section*.2}
\contentsline {paragraph}{Example: Collision Ionization}{2}{section*.3}
\contentsline {subsection}{\numberline {1.2}Emission and Absorption, Radiative Transfer}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Molecular Energy Levels}{3}{subsubsection.1.2.1}
\contentsline {paragraph}{Molecular Vibration}{3}{section*.4}
\contentsline {paragraph}{Molecular Rotation}{3}{section*.5}
\contentsline {subsubsection}{\numberline {1.2.2}Emission and Absorption Processes}{3}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Radiative Transfer}{3}{subsubsection.1.2.3}
\contentsline {subsection}{\numberline {1.3}Continuum Radiation Process}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Fluid Dynamics, Shocks and SNRs}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Shocks, SNRs, HII regions and PDRs}{4}{subsection.1.5}
\contentsline {section}{\numberline {2}STAR FORMATION}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Molecular Clouds}{4}{subsection.2.1}
